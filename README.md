# Summary

Source: https://gitlab.com/agrozyme-docker/ipfs-cluster

Collective pinning and composition for IPFS.

# Settings

Ports:

- `9094`
  - HTTP API
  - tcp:9094 can be exposed when enabling SSL and setting up basic authentication
- `9095`
  - IPFS Proxy endpoint
  - tcp:9095 should not be exposed without an authentication mechanism on top (nginx etc…). By default it provides no authentication nor encryption (similar to IPFS’s tcp:5001)
- `9096`
  - Cluster swarm
  - tcp:9096 is used by the Cluster swarm and protected by the shared secret. It is OK to expose this port (the cluster secret acts as password to interact with it).

# Environment variables

- `IPFS_CLUSTER_PATH`: default value is `/var/lib/ipfs/.ipfs-cluster`
- `IPFS_CLUSTER_BOOTSTRAP`

# Commands

- [ipfs-swarm-key-gen](https://github.com/Kubuxu/go-ipfs-swarm-key-gen): https://github.com/ipfs/go-ipfs/issues/5767#issuecomment-612632099
