#!/usr/bin/lua
local core = require('docker-core')

local function main()
  local run = 'su-exec core /usr/local/bin/ipfs-cluster-service'
  local folder = core.getenv('IPFS_CLUSTER_PATH', '/var/lib/ipfs/.ipfs-cluster')
  local bootstrap = core.getenv('IPFS_CLUSTER_BOOTSTRAP', '')

  core.update_user()
  core.run('mkdir -p %s', folder)
  core.chown(folder)

  if (core.test('! -f %s/service.json', folder)) then
    core.run('%s init', run)
  end

  if ('' ~= bootstrap) then
    bootstrap = '--bootstrap=' .. bootstrap
  end

  core.run('%s daemon %s', run, bootstrap)
end

main()
