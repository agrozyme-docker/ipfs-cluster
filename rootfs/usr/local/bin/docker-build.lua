#!/usr/bin/lua
local core = require('docker-core')

local function load_suffix()
  local arch = core.capture('apk --print-arch')

  if ('x86_64' == arch) then
    return 'amd64'
  elseif ('x86' == arch) then
    return '386'
  elseif ('aarch64' == arch) then
    return 'arm64'
  elseif ('armv7' == arch) then
    return 'arm'
  else
    return arch
  end
end

local function setup_ipfs_cluster(name)
  local version = 'v0.13.0'
  local bin = '/usr/local/bin'
  local suffix = load_suffix()
  local source = string.format('https://dist.ipfs.io/%s/%s/%s_%s_linux-%s.tar.gz', name, version, name, version, suffix)
  local target = '/tmp/' .. name .. '.tar.gz'

  core.run('wget -qO %s %s', target, source)
  core.run('tar -zxf %s -C %s --strip-components=1 %s/%s', target, bin, name, name)
  core.run('chmod +x %s/%s', bin, name)
  core.run('rm -f %s', target)
end

local function main()
  core.run('apk add --no-cache libc6-compat')
  setup_ipfs_cluster('ipfs-cluster-ctl')
  setup_ipfs_cluster('ipfs-cluster-follow')
  setup_ipfs_cluster('ipfs-cluster-service')
end

main()
