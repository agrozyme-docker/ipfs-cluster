#!/bin/bash
set +eux

function source_file() {
  echo "$(readlink -f ${BASH_SOURCE[0]})"
}

function source_path() {
  echo "$(dirname $(source_file))"
}

function setup_alias() {
  local run="$(source_file)"

  alias ipfs-cluster-ctl="${run} ipfs_cluster_ctl"
  alias ipfs-cluster-follow="${run} ipfs_cluster_follow"
  alias ipfs-cluster-service="${run} ipfs_cluster_service"
}

function cli_command() {
  local image="docker.io/agrozyme/ipfs-cluster"
  local command="$(source_path)/docker.do.sh run_command -v ${PWD}:/var/lib/ipfs/.ipfs-cluster $@ ${image} "
  echo "${command}"
}

function ipfs_cluster_ctl() {
  local run="$(cli_command) ipfs-cluster-ctl $@"
  ${run}
}

function ipfs_cluster_follow() {
  local run="$(cli_command) ipfs-cluster-follow $@"
  ${run}
}

function ipfs_cluster_service() {
  local run="$(cli_command) ipfs-cluster-service $@"
  ${run}
}

function main() {
  local call=${1:-}

  if [[ -z $(typeset -F "${call}") ]]; then
    return
  fi

  shift
  ${call} "$@"
}

main "$@"
