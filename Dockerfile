ARG DOCKER_HUB_NAMESPACE=agrozyme
FROM "docker.io/${DOCKER_HUB_NAMESPACE}/alpine"
COPY rootfs /
ENV IPFS_CLUSTER_PATH=/var/lib/ipfs/.ipfs-cluster
RUN set +e -ux && chmod +x /usr/local/bin/* && /usr/local/bin/docker-build.lua
EXPOSE 9094 9095 9096
CMD ["/usr/local/bin/docker-run.lua"]
